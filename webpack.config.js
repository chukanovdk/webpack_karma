const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isCallKeeper = process.env.NODE_ENV === 'cp';
const isCallTraking = !isCallKeeper;
const formAction = process.env.NODE_ENV === 'formAction';
const test = process.env.BABEL_ENV === "test";

module.exports = {
	mode: 'development',
	entry: './src/indexCallTracking.js',
	devtool:'inline-source-map',
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist')
	},

	plugins: [new webpack.ProgressPlugin(), new HtmlWebpackPlugin()],

	module: {
		rules: [
			{
				test: /.(js|jsx)$/,
				include: [path.resolve(__dirname, 'src')],
				loader: 'babel-loader',

				options: {
					plugins: ['syntax-dynamic-import'],

					presets: [
						[
							'@babel/preset-env',
							{
								"targets": {
									"firefox": "21",
									"chrome": "23",
									"safari": "6",
									"ie": "10"
								},
								"useBuiltIns": "usage",
								"corejs": 3,
								"modules": false
							}
						]
					],
					env: {
						test: {
							plugins: ['istanbul']
						}
					}

				}
			}
		]

	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				}
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: true
		}
	},
	watch: true

};
