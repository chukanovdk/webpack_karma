const webpackConfig = require('./webpack.config.js');
/**
 * Этот файл используется для настройки Karma.
 * Нам нужно предварительно обработать файлы с помощью webpack.
 * Для этого мы добавляем webpack в качестве препроцессора и включаем нашу конфигурацию webpack.
 * Мы можем использовать конфигурационный файл webpack в корне проекте, ничего не меняя.
 */
module.exports = function(config) {
    config.set({
        frameworks: ['mocha','jasmine', 'chai'],
        devtool: 'inline-source-map',
        files: ['src/**/*.js','src/**/*.spec.js'],

        preprocessors: {
            'src/**/*.js': ['webpack', 'sourcemap']
        },

        webpack: webpackConfig,
        webpackMiddleware: {
            noInfo:true
        },
        reporters: ['mocha', 'coverage'],

        coverageReporter: {
            dir: './coverage',
            reporters: [
                { type:'html', subdir: 'report-html' },
                { type: 'lcov', subdir: '.' }, { type: 'text-summary' }
            ],
            instrumenterOptions: {
                istanbul: { noCompact:true }
            }
        },

        browsers: ['Chrome'],

        exclude: ['./node_modules']

    })
};
